#!/usr/bin/python

import sys
import time
import socket
from threading import Thread

# Usage: python conn_check.py HOST PORT VALUES
# example:
# python conn_check.py mst11.gain250.com 25 "1 20 40 60 80"

host=sys.argv[1]
port=sys.argv[2]
values=sys.argv[3]
valuesS = values.split()


class Controller():

    def __init__(self):
        self.count_ref = []

    def start(self):

        for i in range(thread_count):
            agent = Agent(self.count_ref)
            agent.setDaemon(True)
            agent.start()

        print 'Started %d threads' % (i + 1)

        #while True:

        time.sleep(1)
        line = 'connects/sec: %s\n' % len(self.count_ref)
        tmp = len(self.count_ref)

        self.count_ref[:] = []
        sys.stdout.write(chr(0x08) * len(line))
        sys.stdout.write(line)

        return tmp


class Agent(Thread):

    def __init__(self, count_ref):
        Thread.__init__(self)
        self.count_ref = count_ref

    def run(self):

        #while True:

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:

            s.connect((host, int(port)))
            self.count_ref.append(1)

            #time.sleep(2)
            #s.shutdown(2)
            #s.close()

            while True:
                pass

        except:
            #print 'SOCKET ERROR\n'
            pass


if __name__ == '__main__':

    f = open("/tmp/connections.log", "w")
    f.write("-1")
    f.close()

    max_threads = 0

    for item in valuesS:
        thread_count = int(item)
        controller = Controller()

        val = controller.start()

        if val > max_threads:
            max_threads = val


        # sleep
        time.sleep(3)

    print "\nMax simultaneous connections: %s" %max_threads

    f = open("/tmp/connections.log", "w")
    f.write("%s" %max_threads)
    f.close()
