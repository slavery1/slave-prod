#!/usr/bin/python
'''
Created on Apr 26, 2013

@author: Majid Hameed
'''

import getopt
import sys
import subprocess
import urlparse
import traceback

PACKAGES = ['httpd', 'php', 'git', 'bind-utils']

SVN_URL = 'svn://dev1.gain250.com/svn/'

G250_INSTALL_PATH = '/usr/local/g250'
APP_DIR = G250_INSTALL_PATH + '/url-handlers'
APP_CONFIG_FILE = APP_DIR + "/url-handler-constants.ini"

LINUX_CMDS = {
                  'install_package':'yum install {0} -y',
                  'remove_package':'yum remove {0} -y',
                  'install_composer': 'curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/',
                  'rename_composer_binary': 'mv -v /usr/local/bin/composer.phar /usr/local/bin/composer',
                  'composer_install_dependencies' : 'composer install -d ' + APP_DIR,
                  'replace_master_server_url' : "sed -i 's/{0}/{1}/g' " + APP_DIR + "/url-handler-constants.ini",
                  'replace_line_cmd' : "sed -i 's/{0}/{1}/g' {2}",
                  'config_daemons' : 'chkconfig --level {0} {1} on',
                  'restart_daemon' : 'service {0} restart',
                  'host_to_ip' :  "/usr/bin/host {0} | /bin/cut -f4 -d' '",
                  'check_line_exists' : "/bin/grep -F '{0}' {1}",
                  'clean_up_dir' : 'rm -rf {0}/*'
                  }

def main():
    optional_param_defaults = {'js_redirect' : 'true', 'bot_filter': 'false'}
    optional_params = ['js_redirect', 'bot_filter']
    required_params = ['master_server_url']
    script_params = required_params + optional_params
    args = sys.argv[1:]
    params = {}
    try:
        option_list, args = getopt.getopt(args, '', [param+'=' for param in script_params])
        for option in option_list:
            option_name = option[0][2:]
            if option_name in script_params:
                params[option_name]=option[1]
            else:
                print 'Unknown/Invalid option passed', option_name, 'passing this option has no effect'
        if len(params)<len(required_params):
            for required_param in required_params:
                if required_param not in params.keys():
                    raise Exception(required_param + ' is required')
        else:
            for optional_param in optional_params:
                if optional_param not in params.keys():
                    print 'Optional param:' + optional_param + " not passed"
                    params[optional_param] = optional_param_defaults[optional_param]
            install_mask_url_handlers(params)
    except:
        print traceback.format_exc()


def install_mask_url_handlers(params):
    status = install_packages()
    status = status and remove_package('httpd-manual')
    status = status and install_composer() and install_app_dependencies() and configure_app(params['master_server_url'], params['js_redirect'], params['bot_filter'])
    status = status and config_daemons() and restart_daemons()
    print status


def clean_up_dir(dir_name):
    print 'Cleaning up', dir_name
    output, error = run_linux_command(LINUX_CMDS['clean_up_dir'].format(dir_name), True)
    print output
    return error==None


def install_packages():
    print 'Installing Packages'
    status = True
    for package in PACKAGES:
        status = status and install_package(package)
    return status 


def install_package(package_name):
    print 'Installing', package_name
    output, error = run_linux_command(LINUX_CMDS['install_package'].format(package_name).split())
    print output
    sys.stdout.flush()
    return error==None


def remove_package(package_name):
    print 'Removing', package_name
    output, error = run_linux_command(LINUX_CMDS['remove_package'].format(package_name).split())
    print output
    return error==None


def make_host_entry(host):
    print 'Making host entry for', host
    host_to_ip_cmd = LINUX_CMDS['host_to_ip'].format(host)
    output, error = run_linux_command(host_to_ip_cmd, True)
    print output
    if error==None:
        ip = output
        line = '\n' + ip.strip() + ' '*3 + host.strip()
        return append_to_file(line, '/etc/hosts')
    return False


def svn_code_checkout(module_name, checkout_dir):
    print 'Code checkout', module_name, 'to', checkout_dir
    svn_cmd = "svn co {0}{1} {2} --username websvn --password _EsBe9! --non-interactive".format(SVN_URL, module_name, checkout_dir)
    output, error = run_linux_command(svn_cmd.split())
    print output
    return error==None


def git_clone_repo(dest_dir):
    git_cmd = 'git clone http://gslave:_paingainrain250\!@dev3.gain250.com:7990/scm/gain/g250-slave.git -b prod ' + dest_dir
    output, error = run_linux_command(git_cmd.split())
    print output
    sys.stdout.flush()
    return error==None


def install_composer():
    print 'Installing Composer'
    output, error = subprocess.Popen(LINUX_CMDS['install_composer'], shell = True, stdout = subprocess.PIPE).communicate()
    print output
    status = error==None
    output, error = run_linux_command(LINUX_CMDS['rename_composer_binary'].split())
    print output
    return status and error==None


def install_app_dependencies():
    print 'Installing App. dependencies using composer'
    output, error = run_linux_command(LINUX_CMDS['composer_install_dependencies'].split())
    print output
    return error==None


def configure_app(master_server_url, js_redirect='true', bot_filter='false'):
    print 'Configuring application'
    cmds = []
    cmds.append(LINUX_CMDS['replace_line_cmd'].format('MASTER_SERVER_URL=http://localhost/oempro'.replace('/','\/'),'MASTER_SERVER_URL=' + master_server_url.replace('/','\/'), APP_CONFIG_FILE))
    cmds.append(LINUX_CMDS['replace_line_cmd'].format('JS_REDIRECT_ENABLED=true','JS_REDIRECT_ENABLED=' + js_redirect.lower(), APP_CONFIG_FILE))
    cmds.append(LINUX_CMDS['replace_line_cmd'].format('BOT_FILTERING_ENABLED=false','BOT_FILTERING_ENABLED=' + bot_filter.lower(), APP_CONFIG_FILE))
    status = run_linux_commands(cmds, True)
    # url=urlparse.urlparse(master_server_url)
    # host=url.netloc
    # status = status and make_host_entry(host)
    return status


def config_daemons():
    print 'Configuring daemons'
    return config_daemon('httpd', 3)


def config_daemon(daemon, level):
    print 'Configuring daemon:', daemon, 'level:', level
    output, error = run_linux_command(LINUX_CMDS['config_daemons'].format(level, daemon).split())
    print output
    return error==None


def restart_daemons():
    print 'Restart daemons'
    return restart_daemon('httpd')        


def restart_daemon(daemon):
    print 'Restarting daemon:', daemon
    output, error = run_linux_command(LINUX_CMDS['restart_daemon'].format(daemon).split())
    print output
    return error==None


def run_linux_command(cmd_with_params, is_shell=False, communicate_input=None, communicate_timeout=3600):
    if not is_shell:
        print str.join(' ',cmd_with_params)
    else:
        print cmd_with_params
    popen = subprocess.Popen(cmd_with_params, shell=is_shell, stdout=subprocess.PIPE)
    return popen.communicate(communicate_input, communicate_timeout)


def run_linux_commands(cmds, is_shell=False):
    status = True
    for cmd in cmds:
        linux_command = cmd.split()
        if is_shell:
            linux_command = cmd
        output, error = run_linux_command(linux_command, is_shell)
        print output
        sys.stdout.flush()
        status = status and error==None
    return status


def append_to_file(line, file_name):
    if is_line_exist(line, file_name):
        print 'line', line, 'already exits in', file_name
        return True
    else:
        print 'Appending line', line, 'to', file_name
        make_cron_entry_cmd = "/bin/echo '{0}' >> {1}".format(line, file_name)
        output, error = run_linux_command(make_cron_entry_cmd, True)
        print output
        return error==None


def is_line_exist(line, file_name):
    check_line_exist_cmd = LINUX_CMDS['check_line_exists'].format(line, file_name)
    output, error = run_linux_command(check_line_exist_cmd, True)
    print output
    return line in output and error==None


if __name__=='__main__':
    main() 