<?php

/**
 *
 * Tracker Image URL Handler - It handles requests for (Campaign) Email Tracking
 *
 * @author Majid Hameed
 * @since Novemeber 25, 2012
 *
 */

use Guzzle\Http\Client;

$scriptName = basename(__FILE__, '.php');

try {
    $_SERVER["SCRIPT_NAME"] =  "/".$scriptName . '.php';
    //curl_post_async($t_processor, $_SERVER, $mask);

    if (LOG_HITS){
        $logger->info($IPAddress . " - GET /" .  $code . ' - ' . 'OPENER - ' . $originalURL);
    }

    if (!BULK_OPEN || !cacheHit($t_processor, $_SERVER, $mask)){
        if (!preg_match("/.*(@gain250.com|\.gain250.com)$/", $decodeEmail)){
            curl_post_async($t_processor, $_SERVER, $mask);  //To log in the server
        }
    }

	//Display Image
	$fileName='eh.gif';
	$filePath="img/$fileName";
	$size=filesize($filePath);
	$contentType='image/gif';

	$logger->debug($scriptName . ' - response - ' . $filePath);

	header("HTTP/1.1 200 OK");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("cache-Control: no-store, no-cache, must-revalidate");
	header("cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header("Content-Type: $contentType");
	header("Content-Length: $size");
	readFile($filePath);
	exit;
} catch (Exception $e) {
	//$logger->error($e->getMessage());
}