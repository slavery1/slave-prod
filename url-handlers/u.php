<?php
use Guzzle\Http\Client;

$scriptName = basename(__FILE__, '.php');

$logger->debug($scriptName . ' - Valid Request - Request Data - ' . json_encode($requestData));

$client = new Client($g_processor);

try {
    $originalURL = getURL($requestData, $client, false, $params, $decodeEmail);
    //var_dump($originalURL);
    //return;

    //var_dump($originalURL);
    //return;

	if ($originalURL==null || $originalURL=='INVALID-URL') {
        if (LOG_HITS){
            $logger->info($IPAddress . " - GET /" .  $code . ' - ' . 'BOT INVALID LINK ID' ); // $_SERVER['HTTP_CF_CONNECTING_IP']);
        }
        $originalURL = '/';
        return;
		//$logger->error("$scriptName - Error INVALID-URL as response. Redirecting to $originalURL");
	}

    $_SERVER["SCRIPT_NAME"] =  "/".$scriptName . '.php';

    if (LOG_HITS){
        $logger->info($IPAddress . " - GET /" .  $code . ' - ' . 'CLICKER - ' . $originalURL);
    }


    if (!BULK_CLICK || !cacheHit($u_processor, $_SERVER, $mask)){
        if (!preg_match("/.*(@gain250.com|\.gain250.com)$/", $decodeEmail)){
            curl_post_async($u_processor, $_SERVER, $mask);
        }
    }

} catch (Exception $e) {
	//$logger->error($e->getMessage());
}
?>
<?php if (!empty($originalURL)): ?>
	<?php if (JS_REDIRECT_ENABLED): ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<script type="text/javascript">
   //setTimeout(redirect, 500);
   redirect();

function redirect(){
    window.location.href = "<?php echo $originalURL; ?>";
}
</script>
<title></title>
</head>
</html>
	<?php else: ?>
		<?php header("Location: $originalURL"); ?>
	<?php endif; ?>
<?php endif; ?>